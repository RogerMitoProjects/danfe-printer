using model;
using System;
using System.IO;
using System.Text;
using System.Drawing;
using System.Drawing.Printing;

namespace pageBuilder
{
    class PageBuilder
    {
        const char PAD = ' ';
        // Defines the space character width according to the font chosen.
        float spaceWidth = 2.0f;
        // The default paper width is setted up to 80 mm.
        int paperWidth;
        private Font defaultFont;
        private Font boldFont;
        private Font smallFont;
        private PaperSettings paperSettings;

        private Bill bill;

        public PageBuilder(Bill bill)
        {
            this.bill = bill;
        }
        public Image getImage(string base64ImageRepresentation)
        {
            byte[] bytes = Convert.FromBase64String(base64ImageRepresentation);
            Image image;
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                image = Image.FromStream(ms);
            }
            return (Image)(new Bitmap(image, new Size(94, 94)));
        }

        public string centeredLine(string line, float lineWidth)
        {
            int remainingSpace = (int)((paperWidth - (lineWidth + 10)) / spaceWidth);
            return line.PadLeft(remainingSpace / 2 + line.Length, PAD).PadRight(remainingSpace / 2 + line.Length, PAD);
        }
        public string emptyCenter(string leftContent, string rightContent, Graphics graphics, Font font)
        {
            // Number of spaces that can be fitted in the currente line.
            int remainingSpace = (int)(((paperWidth - 10) - graphics.MeasureString($"{leftContent}{rightContent}", font).Width) / spaceWidth);
            return $"{leftContent.PadRight((remainingSpace > 0 ? remainingSpace : 0) + leftContent.Length, PAD)}{rightContent}";
        }

        public void buildPage(object content, PrintPageEventArgs e)
        {
            // It applies the paper's settings according to the paper width sent. Case this info isn't provided, 80mm will be set as the default value.
            this.paperSettings = new PaperSettings(bill.paperWidth ?? 80);

            // Fonts
            this.defaultFont = new Font("Arial", paperSettings.defaultFontSize);
            this.boldFont = new Font("Arial", paperSettings.boldFontSize, FontStyle.Bold);
            this.smallFont = new Font("Arial", paperSettings.smalltFontSize);

            // Gets the nearest integer that represents the paper width in pixels.
            // 1 mm -> ~ 3.78 px
            // The default paper width is setted up to 80 mm.
            this.paperWidth = (int)Math.Round(bill.paperWidth * 3.78 ?? 80 * 3.78);

            Graphics graphics = e.Graphics;
            SolidBrush defaultBrush = new SolidBrush(Color.Black);

            // Sets up the amount of spaces which can be fitted in a line according to the chosen font.
            StringFormat f = new StringFormat(StringFormat.GenericTypographic)
            { FormatFlags = StringFormatFlags.MeasureTrailingSpaces };
            this.spaceWidth = graphics.MeasureString(" ", defaultFont, new Point(10, 10), f).Width; // 3

            float fontHeight = defaultFont.GetHeight();
            int offsetX = bill.paperWidth == 58 ? 5 : 10;
            int offsetY = 10;
            string breakDown = string.Empty;
            var sb = new StringBuilder();

            // Centered text.
            StringFormat centered = new StringFormat();
            centered.LineAlignment = StringAlignment.Center;
            centered.Alignment = StringAlignment.Center;

            // Right aligment
            StringFormat alignRight = new StringFormat();
            alignRight.LineAlignment = StringAlignment.Far;
            alignRight.Alignment = StringAlignment.Far;

            Rectangle headerRct = new Rectangle();
            Rectangle listHeaderRct = new Rectangle();
            Rectangle listBodyRct = new Rectangle();
            Rectangle totalRct = new Rectangle();
            Rectangle paymentRct = new Rectangle();


            sb.AppendLine($"{bill.header.cnpj} {bill.header.socialName}");
            sb.AppendLine($"{bill.header.address}");
            sb.AppendLine("Documento Auxiliar de Nota Fiscal de Consumidor Eletrônica");
            sb.AppendLine();
            // Rectangle auto height https://stackoverflow.com/questions/6752520/c-sharp-graphics-drawstring-rectanglef-auto-height-how-to-find-that-height
            headerRct.Location = new Point(offsetX, offsetY);
            headerRct.Size = new Size(paperWidth, ((int)e.Graphics.MeasureString(sb.ToString(), defaultFont, paperWidth, StringFormat.GenericTypographic).Height));
            graphics.DrawString(sb.ToString(), defaultFont, defaultBrush, headerRct, centered);
            sb.Clear();

            // Product list header.
            sb.Append(string.Format("Código").PadRight(paperSettings.headerPadding, ' '));
            sb.Append(string.Format("Qtde").PadRight(paperSettings.headerPadding, ' '));
            sb.Append(string.Format("UN").PadRight(paperSettings.headerPadding, ' '));
            sb.Append(string.Format("Vl Unit").PadRight(paperSettings.headerPadding, ' '));
            sb.Append(string.Format("Vl Total"));
            sb.AppendLine(string.Empty);

            sb.AppendLine(centeredLine("Descrição", graphics.MeasureString("Descrição", defaultFont).Width));

            offsetY = headerRct.Height;

            listHeaderRct.Location = new Point(offsetX, offsetY);
            listHeaderRct.Size = new Size(paperWidth, ((int)e.Graphics.MeasureString(sb.ToString(), boldFont, paperWidth, StringFormat.GenericTypographic).Height));

            graphics.DrawString(sb.ToString(), boldFont, defaultBrush, listHeaderRct);


            sb.Clear();
            sb.AppendLine(string.Empty);
            int remainingSpace = 0;
            string aux;
            foreach (Product product in bill.products)
            {
                aux = $"{product.code.PadRight(paperSettings.horizontalPadding, ' ')}{product.amount.ToString().PadRight(paperSettings.horizontalPadding - product.amount.ToString().Length, ' ')}{"CX".PadRight(paperSettings.horizontalPadding, ' ')}{product.unityValue.ToString().PadRight(paperSettings.horizontalPadding - product.unityValue.ToString().Length, ' ')}{product.totalValue}";

                remainingSpace = (int)graphics.MeasureString(aux, defaultFont).Width;

                sb.AppendLine($"{product.code.PadRight(paperSettings.horizontalPadding, ' ')}{product.amount.ToString().PadRight(paperSettings.horizontalPadding - product.amount.ToString().Length, ' ')}{"CX".PadRight(paperSettings.horizontalPadding, ' ')}{product.unityValue.ToString().PadRight(paperSettings.horizontalPadding - product.unityValue.ToString().Length + (int)((paperWidth - remainingSpace - 10) / spaceWidth), ' ')}{product.totalValue}");
                sb.AppendLine(centeredLine(product.description, graphics.MeasureString(product.description, defaultFont).Width));
            }
            sb.AppendLine(emptyCenter("Qtde. total de itens", bill.products.Length.ToString(), graphics, defaultFont));
            sb.AppendLine(emptyCenter("Valor total R$", bill.totalValue.ToString(), graphics, defaultFont));

            offsetY += 10;

            listBodyRct.Location = new Point(offsetX, offsetY);
            listBodyRct.Size = new Size(paperWidth, ((int)e.Graphics.MeasureString(sb.ToString(), defaultFont, paperWidth, StringFormat.GenericTypographic).Height));
            graphics.DrawString(sb.ToString(), defaultFont, defaultBrush, listBodyRct);


            sb.Clear();
            sb.AppendLine(emptyCenter("Valor a pagar R$", bill.paidValue.ToString(), graphics, defaultFont));

            offsetY += listBodyRct.Height;

            totalRct.Location = new Point(offsetX, offsetY);
            totalRct.Size = new Size(paperWidth, ((int)e.Graphics.MeasureString(sb.ToString(), boldFont, paperWidth, StringFormat.GenericTypographic).Height));
            graphics.DrawString(sb.ToString(), boldFont, defaultBrush, totalRct);

            sb.Clear();
            sb.AppendLine(emptyCenter("FORMA PAGAMENTO", "VALOR PAGO R$", graphics, defaultFont));
            sb.AppendLine(emptyCenter(bill.paymentMethod, bill.paidValue.ToString(), graphics, defaultFont));
            sb.AppendLine(emptyCenter("Troco R$", bill.changeValue.ToString(), graphics, defaultFont));

            offsetY += totalRct.Height;

            paymentRct.Location = new Point(offsetX, offsetY);
            paymentRct.Size = new Size(paperWidth, ((int)e.Graphics.MeasureString(sb.ToString(), defaultFont, paperWidth, StringFormat.GenericTypographic).Height));
            graphics.DrawString(sb.ToString(), defaultFont, defaultBrush, paymentRct);

            // listBodyRct.Height + headerRct.Height + totalRct.Height + offsetY
            Rectangle accessRct = new Rectangle();
            sb.Clear();
            sb.AppendLine("Consulte pela Chave de Acesso em");
            sb.AppendLine("http://nfce.fazenda.rj.gov.br/consulta");
            sb.AppendLine(bill.key);
            sb.AppendLine();

            offsetY += paymentRct.Height;

            accessRct.Location = new Point(offsetX, offsetY);
            accessRct.Size = new Size(paperWidth, ((int)e.Graphics.MeasureString(sb.ToString(), defaultFont, paperWidth, StringFormat.GenericTypographic).Height));
            graphics.DrawString(sb.ToString(), defaultFont, defaultBrush, accessRct, centered);

            offsetY += accessRct.Height;


            Rectangle identRct = new Rectangle();
            sb.Clear();
            sb.AppendLine(bill.client == null ? "CONSUMIDOR NÃO IDENTIFICADO" : $"CONSUMIDOR - CPF {bill.client.cpf} - {bill.client.name} - {bill.client.address}");
            sb.AppendLine();
            sb.AppendLine($"NFC-e nº {bill.billNumber} Série {bill.serie}");
            sb.AppendLine();
            sb.AppendLine($"Protocolo de autorização: {bill.authorizationProtocol}");
            sb.AppendLine();
            sb.AppendLine($"Data de autorização {bill.authorizationDate}");

            // listBodyRct.Height + headerRct.Height + totalRct.Height + paymentRct.Height + accessRct.Height + offsetY
            identRct.Location = new Point(bill.paperWidth == 80 ? 110 : bill.paperWidth == 80 ? 94 + 20 : offsetX, offsetY);
            identRct.Size = new Size(202, ((int)e.Graphics.MeasureString(sb.ToString(), defaultFont, paperWidth, StringFormat.GenericTypographic).Height));
            graphics.DrawString(sb.ToString(), defaultFont, defaultBrush, identRct, bill.paperWidth == 80 ? null : centered);


            offsetY += bill.paperWidth == 80 ? 0 : identRct.Height + 10;
            // QR-Code
            graphics.DrawImage(getImage(bill.qrCode), new Point(bill.paperWidth == 80 ? offsetX : ((this.paperWidth - 94) / 2), offsetY));

            Rectangle extraMessageRct = new Rectangle();
            sb.Clear();
            sb.AppendLine();
            sb.AppendLine($"Tributos Totais Incidentes (Lei Federal 12.741/2012): R$ {bill.taxeValue}");
            sb.AppendLine($"{bill.aditionalMessage}");

            offsetY += bill.paperWidth == 80 ? 94 : identRct.Height;

            extraMessageRct.Location = new Point(offsetX, offsetY);
            extraMessageRct.Size = new Size(paperWidth, ((int)e.Graphics.MeasureString(sb.ToString(), defaultFont, paperWidth, StringFormat.GenericTypographic).Height));
            graphics.DrawString(sb.ToString(), smallFont, defaultBrush, extraMessageRct, centered);
        }
    }
}