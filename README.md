# Características da aplicação

O presente projeto consiste em uma aplicação destinada a impressão do **Documento Auxiliar da Nota Fiscal Eletrônica - DANFE** para nota fiscal de consumidor eletrônica modelo 65 (NFC-e), também conhecido como **DANFE NFC-e**.

Segundo definição da Secretaria de Estado da Fazenda - SEFAZ:

> O DANFE (Documento Auxiliar da Nota Fiscal Eletrônica) é um documento fiscal auxiliar, que pode ser impresso em papel. O DANFE não é nota fiscal, nem a substitui, servindo apenas como instrumento auxiliar para consulta da NF-e, pois contém a chave de acesso da NF-e, que permite ao detentor desse documento confirmar, através das páginas da Secretaria de Fazenda Estadual ou da Receita Federal do Brasil(RFB), a efetiva existência de uma NF-e que tenha tido seu uso regularmente autorizado.

Mais especificamente para o DANFE NFC-e:

> O DANFE NFC-e é um documento fiscal auxiliar, sendo apenas uma representação simplificada da transação de venda no varejo, que pode ser impressa, de forma a facilitar a consulta do documento fiscal eletrônico, no ambiente da SEFAZ, pelo consumidor final. A impressão do DANFE NFC-e é efetuada diretamente pelo aplicativo do contribuinte em impressora comum (não fiscal), com base nas informações do arquivo eletrônico XML da NFC-e.

A aplicação funciona no esquema cliente-servidor utilizando Websockets baseado no protocolo TCP.
Para impressão do DANFE, o cliente deve enviar os dados da nota em formato JSON conforme a estrutura de dados apresentada adiante.

![Fluxograma de funcionamento da aplicação](how-it-works.png)
_Figura 1 - Funcionamento da aplicação._

A Figura 1 apresenta a aplicação mais comum para o serviço desenvolvido que consiste em cliente e servidor em execução local.

## Layout do DANFE

A aplicação segue o layout establecido pela SEFAZ conforme as Figuras 2 e 3.
Em suma o que difere os modelos apresentados nas Figuras 2 e 3 é a localização do QR code que pode ser centralizado ou alinhado à esquerda.
![modelo de DANFE NFC-e com QR Code na lateral](danfe-model-side-qr-code.png)
_Figura 2 - Modelo DANFE NFC-e com QR Code na lateral_

![modelo de DANFE NFC-e com QR Code na lateral](danfe-model-centered-qr-code.png)
_Figura 3 - Modelo DANFE NFC-e com QR Code centralizado_

**A atual versão da aplicação suporta ambos os estilos sendo que para o papel com largura de 80mm o QR code é sempre renderizado alinhado à esquerda enquanto que para o papel com largura de 58mm o QR code é renderizado centralizado devido ao espaço horizontal limitado.**

As Figuras 4 e 5 apresentam exemplos de DANFE gerados pela aplicação em folhas com 80 mm e 50 mm de largura respectivamente.

Exemplo de DANFE gerada pela aplicação para papel 80mm com QR Code alinhado à esquerda:

<img src="80mm.png" alt="DANFE modelo 80 mm" width="400">

<p  align="center">Figura 4 - DANFE gerada pela aplicação em papel de 80 mm.</p>

Exemplo de DANFE gerada pela aplicação para papel 58mm com QR Code centralizado:

<img src="58mm.png" alt="DANFE modelo 58 mm" width="400">
<p  align="center">Figura 5 - DANFE gerada pela aplicação em papel de 58 mm.</p>

# Documentação técnica

## Estrutura dos dados de impressão

### Diagrama de impressão

A montagem do DANFE é realizada por meio da divisão do documento em regiões, mais especificamente retângulos disponibilizados pelo pacote `System.Drawing`. A necessidade de seguir uma formatação específica irá implicar na criação de um retângulo dedicado para tal. Por exemplo, a criação do seguinte trecho implicaria na criação de dois retângulos paralelos, um para a formatação em negrito e outro para a formatação normal.

<center>
<p>
<strong>Data de autorização</strong> &nbsp &nbsp &nbsp 03/01/2020 21:45:02
</p>
</center>

Seguindo esta ideia, a figura abaixo apresenta a divisão de regiões utilizadas pelo algoritmo de impressão.

![Diagrama de impressão em que é possível conferir a estrutura de retângulos utilizada na construção do DANFE](document-priting-diagram.png)

- headerRct: Informações do cabeçalho do DANFE;
- listHeaderRct: Cabeçalho da listagem de produtos;
- listBodyRct: Listagem de produtos, além de total de itens, valor total da nota e valor do desconto;
- totalRct: Infomações de totais do DANFE;
- paymentRct: Informações de pagamento;
- accessRct: Informações dos dados de consulta na SEFAZ;
- identRct: Informações de identificação do consumidor e da própria NFC-e;
- extraMessageRct: Mensagens adicionais de interesse do contribuinte.

### Estrutura JSON de dados

O diretório `assets/` na raiz do projeto contém alguns arquivos-exemplo de NFC-e de acordo com a estrutura esperada pela aplicação.
A seguir são exibidos os principais trechos da estrutura JSON que deve ser enviada pelo cliente para a impressão do DANFE.

Cabeçalho - Os dados desta seção são renderizados no retâgulo **headerRtc**.

- **cnpj** `string` CNPJ do emitente;
- **socialName** `string` Razão social do emitente;
- **address** `string` Endereço do emitente formatado conforme apresentado pelo manual da SEFAZ;

```json
{
  "header": {
    "cnpj": "29.663.193/0001-42",
    "socialName": "Nome Social da Empresa",
    "address": "Rua 25 de Março, 299, Centro - Buenópolis MG"
  }
}
```

Produtos - Array de produtos da NFC-e.

- **code** `string` Código do produto;
- **description** `string` Descrição do produto;
- **unity** `string` Unidade de venda;
- **amount** `float` Quantidade vendida do produto;
- **unityValue** `decimal` Valor unitário;
- **totalValue** `decimal` Valor total.

```json
{
  "products": [
    {
      "code": "123900",
      "description": "LEITE EM PÓ NINHO 500 G",
      "unity": "LT",
      "amount": 2,
      "unityValue": 11.9,
      "totalValue": 23.8
    },
    {
      "code": "100900",
      "description": "BISCOITO MAIZENA AYMORÉ 500 G",
      "unity": "PT",
      "amount": 10,
      "unityValue": 2.5,
      "totalValue": 255.0
    },
    {
      "code": "100905",
      "description": "CAFÉ LETÍCIA EXTRA FORTE 250 G",
      "unity": "PT",
      "amount": 1,
      "unityValue": 3.86,
      "totalValue": 3.86
    }
  ]
}
```

Dados do consumidor (opcional)

- **name** `string` Nome do consumidor;
- **cpf** `string` CPF do consumidor com máscara aplicada;
- **address** `string` Endereço do consumidor formatado conforme apresentado pelo manual da SEFAZ;

```json
{
  "client": {
    "name": "Rogério de Oliveira Batista",
    "cpf": "655.128.460-43",
    "address": "Rua Célio de Castro, 350, Floresta, Belo Horizonte - MG"
  }
}
```

**\*Obs.:** Caso os dados do consumidor não sejam informados, a tag `client` deve ser enviada com valor nulo da seguinte forma:

```json
{
  "client": null
}
```

Demais campos

- **paperWidth** `int` Largura do papel utilizado na impressão do DANFE (58 mm ou 80 mm );
- **printerName** `string` Nome da impressora utilizada para impressão do DANFE;
- **paymentMethod** `string` Método de pagamento;
- **totalValue** `decimal` Valor total da nota;
- **paidValue** `decimal` Valor pago pelo consumidor;
- **changeValue** `decimal` Valor do troco;
- **key** `string` Chave da NFC-e (44 digítos);
- **billNumber** `string` Número da NFC-e;
- **serie** `string` Série da NFC-e;
- **authorizationProtocol** `string` Protocolo de autorização fornecido pela SEFAZ;
- **authorizationDate** `string` Data de autorização da NFC-e;
- **discountValue** `decimal` Valor do desconto fornecido na compra;
- **taxeValue** `decimal` Total de impostos aproximados da compra IBPT (opcional);
- **aditionalMessage** `string` Menssagem adicional de interesse do consumidor (opcional);
- **qrCode** `string` Imagem do QR Code gerado codificada em base64.

```json
{
  "paperWidth": 58,
  "printerName": "Microsoft Print to PDF",
  "paymentMethod": "Dinheiro",
  "totalValue": 52.86,
  "paidValue": 52.86,
  "changeValue": 0.0,
  "key": "31201233600466000187651000000001171000002617",
  "billNumber": "0000001",
  "serie": "001",
  "authorizationProtocol": "314 1300004001 80",
  "authorizationDate": "10/03/2020 22:01:55",
  "discountValue": 0.0,
  "taxeValue": 0.0,
  "aditionalMessage": "Obrigado pela preferência!",
  "qrCode": "UVIgQ29kZSBpbWFnZQ=="
}
```
### Listagem de impressoras
Para obter a lista de impressoras disponíveis, a mensagem `printers` deve ser enviada para o módulo de impressão. A resposta é retornada em formato de array de string, como por exemplo:

```json
["Generic CUPS-BRF Printer", "SCX-3400-Series"]
```
Neste exemplo duas impressoras foram localizadas pelo módulo de impressão. A aplicação cliente deve armazenar essa estrutura e enviar somente a impressora alvo no campo `printerName` presente na estrutura JSON apresentada anteriormente.



## Dependências

```
dotnet add package System.Drawing.Common --version 5.0.0
```

## Execução

A aplicação é iniciada por meio do seguinte comando:

```bash
dotnet run
```

**Observação: A aplicação usa por padrão a porta `5005`, dessa forma é necessário garantir que tal porta não esteja disponível na máquina cliente.**

## Testes manuais

O projeto inclui uma pequena aplicação cliente para testes no caminho `test\client`. É importante salientar que, a aplicação cliente deve sempre ser iniciada somente após a inicialização do servidor.

**Observação: Antes de testar o projeto, lembre-se de definir corretamente nos arquivos de exemplo JSON localizados no diretório `assets`, o nome da impressora disponível na máquina utilizada na execução.**

Ex:

```json
{ "printerName": "Microsoft Print to PDF" }
```

Testes manuais também podem ser executados utilizando ferramentas de terceiros como o [Hercules](https://www.hw-group.com/software/hercules-setup-utility) por exemplo. Essa ferramenta permite o envio de arquivos JSON de teste diretamente para serviço de impressão.

![Teste de conexão utilizando o software Hercules](connection-test.png)
_Figura 5 - Hercules conectado ao serviço de impressão._

## Testes automáticos (testes de unidade)

Pendente.

## Build

O geração do executável da aplicação deve ser feita de acordo com a arquitetura do sistema operacional desejado. Os exemplos a seguir ilustram a publicação para ambiente Windows e Linux ambos 64 bits. O executável de saída será gerado no diretório `./dist`. Para mais detalhes consulte [Dotnet Publish](https://docs.microsoft.com/en-us/dotnet/core/deploying/deploy-with-cli).

**Observação: Para que a execução da aplicação não dependa do framework .net instalado na máquina do cliente, o parâmetro `--self-contained` deve ser sempre defindo como `true`.**

### Windows

```bash
dotnet publish -r win-x64 -p:PublishSingleFile=true --self-contained true --output ./dist
```

### Linux

```bash
dotnet publish -r linux-x64 -p:PublishSingleFile=true --self-contained true --output ./dist
```

## Melhorias

- Criação de instalador para distribuição;
- Permitir seleção de layout de impressão de QR code(alinhado à esquerda ou centralizado) para papel de 80 mm.
- Elaboração de testes de unidade;
- Escolha de porta alternativa caso a porta padrão definida pelo serviço esteja em uso.
