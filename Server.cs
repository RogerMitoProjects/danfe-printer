using System;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Net.Sockets;
using System.Drawing.Printing;
using System.Collections.Generic;

using model;
using pageBuilder;
class Server
{
    private static TcpListener server { get; set; }
    private const int PORT = 5005;
    private const string IP = "127.0.0.1";
    private int attempts = 0;
    private PrintDocument pd;
    private PageBuilder pageBuilder;
    private string ListPrinters()
    {
        List<string> printers = new List<string>();

        foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
        {
            printers.Add(printer);
        }

        return JsonSerializer.Serialize(printers);
    }
    public void Start()
    {
        try
        {
            IPAddress address = IPAddress.Parse(IP);
            server = new TcpListener(address, PORT);
            // Start listening for client requests.
            server.Start();
            Console.WriteLine("Server started...");
            // Buffer for reading data.
            Byte[] bytes = new Byte[4096];
            string data = null;
            if (server != null)
            {
                // Enter the listening loop.
                while (true)
                {
                    Console.WriteLine("Waiting for client...");
                    // Accepts a pending connection request as an asynchronous operation.
                    var clientTask = server.AcceptTcpClientAsync(); // Get the client.
                    if (clientTask.Result != null)
                    {
                        Console.WriteLine("Client connected. Waiting for data.");
                        var client = clientTask.Result;
                        // Get a stream object for reading and writing.
                        NetworkStream stream = client.GetStream();
                        int i;
                        // Loop to receive all the data sent by the client.
                        while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                        {
                            data = UTF8Encoding.UTF8.GetString(bytes, 0, i);
                            // Send back all available printers;
                            if (data.StartsWith("printers"))
                            {
                                string printers = ListPrinters();
                                byte[] send = System.Text.Encoding.ASCII.GetBytes(printers);
                                stream.Write(send, 0, send.Length);
                            }
                        }
                        if (!string.IsNullOrEmpty(data))
                        {
                            // Convert the received data to Bill object.
                            var options = new JsonSerializerOptions
                            {
                                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                                WriteIndented = true
                            };
                            Bill bill = JsonSerializer.Deserialize<Bill>(data, options);

                            pd = new PrintDocument();
                            pd.PrinterSettings.PrinterName = bill.printerName;
                            pd.DefaultPageSettings.Margins = new Margins(1, 1, 1, 1);

                            pageBuilder = new PageBuilder(bill);
                            pd.PrintPage += new PrintPageEventHandler(pageBuilder.buildPage);
                            pd.Print();
                        }
                        // Shutdown and end connection.
                        client.Close();
                    }
                }
            }

        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
            attempts++;
        }
        finally
        {
            // If something goes wrong, it attempts to start the socket service up to 3 times.
            if (attempts < 4)
            {
                this.Start();
            }
            else
            {
                // Stop listening for new clients.
                server.Stop();
            }
        }
    }
}