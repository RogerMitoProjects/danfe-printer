namespace model
{
    class Bill
    {
        public string paymentMethod { get; set; }
        public decimal totalValue { get; set; }
        public decimal paidValue { get; set; }
        public decimal changeValue { get; set; }
        public decimal taxeValue { get; set; }
        public string key { get; set; }
        public string billNumber { get; set; }
        public string authorizationDate { get; set; }
        public decimal discountValue { get; set; }
        public string qrCode { get; set; }
        public string serie { get; set; }
        public string authorizationProtocol { get; set; }
        public string aditionalMessage { get; set; }
        // Paper width in mm.
        public int? paperWidth { get; set; }

        public string printerName { get; }

        // Product list
        public Product[] products { get; set; }
        // Bill header
        public Header header { get; set; }
        // Client info
        public Client client { get; set; }
    }
}