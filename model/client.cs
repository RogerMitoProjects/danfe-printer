namespace model
{
    class Client
    {
        public string cpf { get; set; }
        public string name { get; set; }
        public string address { get; set; }
    }
}