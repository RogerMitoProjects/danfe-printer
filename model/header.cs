namespace model
{
    class Header
    {
        public string cnpj { get; set; }
        public string socialName { get; set; }
        public string address { get; set; }
    }
}