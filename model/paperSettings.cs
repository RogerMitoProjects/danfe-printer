namespace model
{
    class PaperSettings
    {
        public int horizontalPadding { get; }
        public int defaultFontSize { get; }
        public int smalltFontSize { get; }
        public int boldFontSize { get; }
        public int headerPadding { get; }

        public PaperSettings(int paperWidth)
        {
            switch (paperWidth)
            {
                case 58:
                    this.headerPadding = 14;
                    this.smalltFontSize = 5;
                    this.defaultFontSize = 6;
                    this.boldFontSize = 6;
                    this.horizontalPadding = 16;
                    break;
                case 80:
                    this.headerPadding = 18;
                    this.boldFontSize = 7;
                    this.defaultFontSize = 7;
                    this.smalltFontSize = 6;
                    this.horizontalPadding = 20;
                    break;

                default:
                    this.headerPadding = 15;
                    this.boldFontSize = 7;
                    this.defaultFontSize = 7;
                    this.smalltFontSize = 6;
                    this.horizontalPadding = 10;
                    break;
            }
        }
    }
}