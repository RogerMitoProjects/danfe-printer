namespace model
{
    class Product
    {
        public string code { get; set; }
        public string description { get; set; }
        public int amount { get; set; }
        public decimal unityValue { get; set; }
        public decimal totalValue { get; set; }
    }
}