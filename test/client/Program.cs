﻿using System;
using System.IO;
using System.Text;
using System.Net.Sockets;

namespace client
{
    class Program
    {
        private const int PORT = 5005;
        private const string IP = "127.0.0.1";
        private static TcpClient client;

        public void Start()
        {
            try
            {

                // Create a UTF-8 encoding.
                UTF8Encoding utf8 = new UTF8Encoding();
                // Read the bill stored in a JSON file and convert it into a string.
                string jsonString = File.ReadAllText("../../assets/bill-with-client-58mm.json");

                client = new TcpClient(IP, PORT);
                // Converting string into bytes.
                byte[] sendData = UTF8Encoding.UTF8.GetBytes(jsonString);

                NetworkStream stream = client.GetStream();
                // Send the data to the server.
                stream.Write(sendData, 0, sendData.Length);

                stream.Close();
                client.Close();
            }
            catch (System.Exception e)
            {
                Console.WriteLine("An error happened in the client: {0}", e.ToString());
            }
        }
        static void Main(string[] args)
        {
            Program client = new Program();
            client.Start();
        }
    }
}
